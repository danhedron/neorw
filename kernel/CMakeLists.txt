###############################################################################
# Game Kernel Library
###############################################################################

SET(PROJECT_NAME GameKernel)
SET(PROJECT_FLAGS "-std=c++11")
SET(PROJECT_INTERFACE_PATHS
	${CMAKE_CURRENT_SOURCE_DIR}/source/
)

SET(PROJECT_SOURCE
	source/core.hpp
	source/core/types.hpp
	source/core/debug.hpp

	# Memory

	source/memory.hpp
	source/memory/memory_allocator.hpp
	source/memory/memory_allocator.cpp
	source/memory/system_memory_allocator.hpp
	source/memory/system_memory_allocator.cpp
	source/memory/memory_pool.hpp
	source/memory/memory_pool.cpp
	source/memory/memory_utils.hpp
	source/memory/stl_allocator.hpp
	source/memory/stl_allocator.cpp

	# Filesystem

	source/fs.hpp
	source/fs/file_info.hpp
	source/fs/file_info.cpp

	# Job System

	source/job_system.hpp
	source/job_system/job_task.hpp
	source/job_system/job_task.cpp
	source/job_system/thread_pool.hpp
	source/job_system/thread_pool.cpp
	source/job_system/job_queue.hpp
	source/job_system/job_queue.cpp
)

SET(PROJECT_TEST
	tests/test_main.hpp
	tests/test_main.cpp

	tests/test_memory_allocator.cpp
	tests/test_memory_pool.cpp

	tests/test_file.cpp

	tests/test_thread_pool.cpp
)

SET(PROJECT_INCLUDE_DIRS ${CMAKE_CURRENT_LIST_DIR}/source)

Declare_Library()


