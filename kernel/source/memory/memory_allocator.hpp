/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef MEMORY_ALLOCATOR_HPP
#define MEMORY_ALLOCATOR_HPP

#include <core/types.hpp>
#include <memory>

namespace gk
{
	/**
	 * The result of an allocation
	 */
	struct mem_block
	{
		/**
		 * @brief ptr Address of the allocated memory
		 */
		void* ptr;
		/**
		 * @brief size Number of bytes allocated
		 */
		GKSize_t size;
	};

	/**
	 * Provides an easy to use abstraction over the memory block interface
	 */
	template <typename T>
	class mem_owned
	{
	public:
		mem_owned()
			: m_mem{ nullptr, 0 }
		{ }

		mem_owned(mem_owned<T>&& prev)
			: m_mem(prev.m_mem)
		{ }

		mem_owned(const mem_block& mem)
			: m_mem(mem)
		{ }

		mem_owned<T>& operator = (mem_owned<T> &&rhs)
		{
			m_mem = rhs.m_mem;
			// The original block is now done.
			rhs.m_mem = { nullptr, 0 };
		}

		/**
		 * @brief operator T * Provides an imlpicit cast to the contained type
		 */
		operator T*() const { return static_cast<T*>(m_mem.ptr); }
		T* operator ->() const { return static_cast<T*>(m_mem.ptr); }

		const mem_block& mem() const { return m_mem; }

	private:
		mem_block				m_mem;
	};

	/**
	 * Memory allocator interface
	 *
	 * The interface provided by a memory_allocator is different from typical
	 * malloc/free allocators. They return allocations as blocks, which contain the
	 * size of the allocation.
	 *
	 * If you need to provide an allocator to a library that uses the malloc/free model,
	 * there is a wrapper that will handle the mapping of ptrs to mem_block.
	 */
	class memory_allocator
	{
	public:
		virtual ~memory_allocator() { }

		/**
		 * @brief allocate Attempts to allocate a block of the given size.
		 * @param size The number of bytes to allocate
		 * @return The result of the allocation
		 */
		virtual mem_block allocate(GKSize_t size) = 0;

		/**
		 * Free memory previously allocated from this allocator
		 */
		virtual void deallocate(const mem_block& mem) = 0;
	};

}

#endif
