/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef STLALLOCATOR_HPP
#define STLALLOCATOR_HPP
#include <memory/memory_allocator.hpp>

#include <deque>
#include <vector>

namespace gk {
	/**
	 * Provides compatability with the STL allocator interface.
	 */
	template <class T>
	class stl_allocator {
	public:
		typedef T	value_type;
		typedef T*	pointer;
		typedef const T*	const_pointer;
		typedef T&	reference;
		typedef const T&	const_reference;

		stl_allocator(gk::memory_allocator* alloc)
			: m_allocator(alloc) { }

		template <class U> stl_allocator(const stl_allocator<U>& other)
			: m_allocator(other.allocator()) { }

		T* allocate(std::size_t n) {
			GKSize_t allocSize = n * sizeof(T);
			auto mem = static_cast<T*>(m_allocator->allocate(allocSize).ptr);
			return mem;
		}

		void deallocate(T* p, std::size_t n) {
			// Thankfully, since we have T and n, we know exactly how large
			// an allocated block should be
			m_allocator->deallocate({p, n * sizeof(T)});
		}

		template <class...Args>
		T* construct(T* p, Args&&... args)
		{
			return new (p) T (std::forward<Args>(args)...);
		}

		void destroy(T* p) {
			p->~T();
		}

		template <class U>
		struct rebind { using other = stl_allocator<U>; };

		gk::memory_allocator* allocator() const {
			return m_allocator;
		}
	private:
		gk::memory_allocator* m_allocator;
	};

	// Specialise some STL containers on our allocator
	// so we can make sure we don't accidenally
	// allocate without going through one of our own.

	template <class T>
	using vector = std::vector<T, stl_allocator<T>>;

	template <class T>
	using queue = std::deque<T, stl_allocator<T>>;

}

#endif
