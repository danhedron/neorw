/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef MEMORY_POOL_HPP
#define MEMORY_POOL_HPP

#include <core/types.hpp>
#include <memory/memory_allocator.hpp>

namespace gk
{
	/**
	 * Specialised memory allocator that performs allocates from another
	 * allocator.
	 *
	 * This allocator supports having a fixed size, as well as tracking
	 * statistics about the size of allocations within the pool.
	 *
	 * The number of allocated bytes is calculated from the bytes requested,
	 * not the total number of bytes allocated by the underlying allocator.
	 */
	class memory_pool : public memory_allocator
	{
	public:
		static constexpr GKSize_t unbounded = -1;
		static constexpr GKSize_t name_max_size = 16;

		memory_pool(memory_allocator *pAllocator
					, GKSize_t szPool
					, const GKChar *strName = nullptr);
		virtual ~memory_pool();

		virtual mem_block allocate(GKSize_t size) override;
		virtual void deallocate(const mem_block &mem) override;

		GKSize_t current_allocation() const;
		GKSize_t maximum_allocation() const;

		GKSize_t capacity() const;

		const GKChar* name() const;
	private:
		memory_allocator *m_allocator;
		GKSize_t m_maxSize;
		GKSize_t m_currentSize;
		GKSize_t m_maximumSize;
		GKChar m_name[name_max_size];
	};
}

#endif
