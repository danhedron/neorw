#include "memory_pool.hpp"

#include <cstring>

using namespace gk;

// Amazing.
constexpr GKSize_t memory_pool::unbounded;

memory_pool::memory_pool(memory_allocator *pAllocator
		, GKSize_t szPool
		, const char* strName)
	: m_allocator(pAllocator)
	, m_maxSize(szPool)
	, m_currentSize(0)
	, m_maximumSize(0)
{
	if (strName != nullptr) {
		strncpy(m_name, strName, name_max_size - 1);
		m_name[name_max_size - 1] = '\0';
	}
}

memory_pool::~memory_pool()
{
}

mem_block memory_pool::allocate(GKSize_t size)
{
	GKSize_t allocSize = size;
	if ( m_currentSize + allocSize > m_maxSize ) {
		return { nullptr, size };
	}
	mem_block mem = m_allocator->allocate(allocSize);
	if ( mem.ptr != nullptr ) {
		m_currentSize += allocSize;
		m_maximumSize = std::max(m_maximumSize, m_currentSize);
	}
	return mem;
}

void memory_pool::deallocate(const mem_block& mem)
{
	if ( mem.ptr != nullptr )
	{
		GKSize_t allocSize = mem.size;
		m_currentSize -= allocSize;
		m_allocator->deallocate(mem);
	}
}

GKSize_t memory_pool::current_allocation() const
{
	return m_currentSize;
}

GKSize_t memory_pool::maximum_allocation() const
{
	return m_maximumSize;
}

GKSize_t memory_pool::capacity() const
{
	return m_maxSize;
}

const char *memory_pool::name() const
{
	return m_name;
}
