/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef MEMORY_UTILS_HPP
#define MEMORY_UTILS_HPP

#include <memory/memory_allocator.hpp>

namespace gk
{
	/**
	 * Allocate the memory for and construct an
	 * instance of type T. This returns a mem_owned
	 * object that provides the type annotated pointer
	 * and stores the data required to free the memory.
	 */
	template <class T, class...Args>
	mem_owned<T> construct(memory_allocator& alloc, Args&&... args)
	{
		auto mem = alloc.allocate(sizeof(T));
		new (mem.ptr) T (std::forward<Args>(args)...);
		return mem;
	}

	template <class T>
	void deconstruct(memory_allocator& alloc, const mem_owned<T> &instance)
	{
		instance->~T();
		alloc.deallocate(instance.mem());
	}
}

#endif
