#include <memory/system_memory_allocator.hpp>
#include <core.hpp>

#include <cstdlib>

using namespace gk;

mem_block system_memory_allocator::allocate(GKSize_t size)
{
	return { std::malloc(size), size };
}

void system_memory_allocator::deallocate(const mem_block& mem)
{
	GK_ASSERT(mem.ptr != nullptr, "block pointer is null");
	GK_ASSERT(mem.size > 0, "block has no size");

	std::free(mem.ptr);
}
