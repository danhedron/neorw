#include "thread_pool.hpp"
#include "job_task.hpp"
#include <core/debug.hpp>

#include <functional>
#include <atomic>

using namespace gk;

static std::mutex g_wait_mutex;
#define MUTEX_GUARD(mutex) \
	std::lock_guard<decltype(mutex)> lock_##mutex(mutex);

class wait_task : public job_task {
public:
	wait_task(GKSize_t& counter, GKSize_t& exited)
		: job_task("Wait")
		, m_counter(counter)
		, m_exited(exited) {

	}

	virtual void work() override {
		{
			MUTEX_GUARD(g_wait_mutex);
			m_counter -= 1;
		}

		while (true) {
			MUTEX_GUARD(g_wait_mutex);
			if (m_counter == 0) {
				m_exited++;
				return;
			}
		}
	}
private:
	GKSize_t& m_counter;
	GKSize_t& m_exited;
};

thread_pool::~thread_pool()
{
	m_shouldContinue = false;
	for (GKSize_t t = 0; t < m_threads.size(); ++t) {
		m_threads[t].join();
	}
}

void thread_pool::initialize(GKSize_t threadshint)
{
	m_shouldContinue = true;

	GKSize_t threadcount = threadshint;
	if (threadcount == 0) {
		threadcount = std::thread::hardware_concurrency();
	}
	GK_ASSERT(threadcount > 0, "threadcount must be non-zero");

	m_threads.reserve(threadcount);

	for (GKSize_t t = 0; t < threadcount; ++t) {
		m_threads.push_back(std::thread(thread_run, this));
	}
}

void thread_pool::add_work(job_task *work)
{
	GK_ASSERT(m_threads.size() > 0, "thread_pool::initialize must be called");

	MUTEX_GUARD(m_mutex);
	m_work.push(work);
}

GKSize_t thread_pool::worker_count() const
{
	return m_threads.size();
}

void thread_pool::wait()
{
	GKSize_t fence = worker_count();
	GKSize_t finished = 0;

	std::vector<wait_task> tasks;
	tasks.reserve(worker_count());

	// Insert a blocking task for each worker
	for (GKSize_t w = 0; w < worker_count(); ++w) {
		tasks.emplace_back(wait_task(fence, finished));
	}

	for (wait_task& task : tasks) {
		add_work(&task);
	}

	while (true) {
		{
			if (fence == 0 && finished == worker_count()) {
				break;
			}
		}
		std::this_thread::yield();
	}
}

void thread_pool::thread_run(thread_pool *self)
{
	while (self->m_shouldContinue) {
		job_task* work = nullptr;

		{
			std::lock_guard<std::mutex> guard(self->m_mutex);
			if (! self->m_work.empty()) {
				work = self->m_work.front();
				self->m_work.pop();
			}
		}

		// Check that we got any work
		if (work != nullptr) {
	#if GK_JOB_DEBUG
			GK_LOG("Starting work: %s", work->task_name());
	#endif
			work->work();
	#if GK_JOB_DEBUG
			GK_LOG("Finished work: %s", work->task_name());
	#endif
		}
	}
}
