/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef THREAD_POOL_HPP
#define THREAD_POOL_HPP

#include <core/types.hpp>

#include <vector>
#include <thread>
#include <mutex>
#include <queue>

namespace gk {
	class job_task;

	/**
	 * A pool of threads that will perform the work pushed to the pool
	 */
	class thread_pool {
	public:
		typedef std::thread thread_type;
		typedef std::vector<thread_type> thread_list;
		typedef std::queue<job_task*> task_queue;

		thread_pool()
			: m_shouldContinue(false)
		{ }
		~thread_pool();

		/**
		 * @brief initialize the thread pool
		 * @param threads number of threads. 0 == autodetect.
		 */
		void initialize(GKSize_t threads);

		/**
		 * Push work into the pool to be executed by a worker
		 * @brief add_work
		 * @param work
		 */
		void add_work(job_task *work);

		/**
		 * @brief worker_count fetch the number of worker threads
		 */
		GKSize_t worker_count() const;

		/**
		 * Waits for all work in the work pool to be consumed
		 */
		void wait();
	private:
		thread_list m_threads;
		task_queue m_work;
		std::mutex m_mutex;
		bool m_shouldContinue;
		static void thread_run(thread_pool*);
	};
}

#endif
