#include "file_info.hpp"

#include <cstring>
#if NEORW_LINUX
#include <sys/stat.h>
#include <errno.h>
#else
#error "Missing implementation"
#endif

using namespace gk;

file_info::file_info(const GKChar *strPath)
{
	GK_ASSERT(strlen(strPath) < path_max_length - 1, "path too long");
	GK_ASSERT(strPath != nullptr, "strPath must be non-null");
	std::strncpy(m_filePath, strPath, path_max_length - 1);
	m_filePath[path_max_length - 1] = '\0';

	// Sanitise the path
	int len;
	len = strlen(m_filePath);
	if (len > 1)
	{
		// Remove a trailing slash
		if (m_filePath[len-1] == '/') {
			m_filePath[len-1] = '\0';
		}
	}
}

const GKChar *file_info::path() const
{
	return m_filePath;
}

file_info file_info::child(const GKChar *name) const
{
	GK_ASSERT(strlen(m_filePath)+strlen(name)+1 < path_max_length, "path too long");
	char new_path [path_max_length];
	strcpy(new_path, m_filePath);
	int len;
	len = strlen(new_path);
	if (len > 1) {
		strncat(new_path, "/", path_max_length-len);
	}
	strncat(new_path, name, path_max_length-len-1);
	return file_info(new_path);
}

bool file_info::exists()
{
#if NEORW_LINUX
	const char *strSysPath = m_filePath;
	struct stat stat_buff;

	return (stat(strSysPath, &stat_buff) == 0);
#else
#error "Unimplemented"
#endif
}
