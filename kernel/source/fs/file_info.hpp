/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef FILE_HPP
#define FILE_HPP

#include <core/types.hpp>
#include <core/debug.hpp>

namespace gk
{
	/**
	 * A file on the system filesystem
	 */
	class file_info
	{
	public:
		/**
		 * Maximum allowed path length.
		 */
		static constexpr GKSize_t path_max_length = 512;

		/**
		 * @brief Creates a file_info for a system path
		 * @param strPath
		 */
		file_info(const GKChar *strPath);

		/**
		 * @brief returns the sanitised path
		 */
		const GKChar *path() const;

		/**
		 * @brief returns a file_info representing a child of this file
		 */
		file_info child(const GKChar *name) const;

		bool exists();

	private:
		GKChar m_filePath [path_max_length];
	};
}

#endif
