#include <gtest/gtest.h>

#include <job_system.hpp>

using namespace gk;

class test_work : public job_task {
public:
	test_work(int time, bool *signal)
		: job_task("testjob")
		, time(time) {
		this->signal = signal;
		*this->signal = false;
	}

	virtual void work() override {
		std::this_thread::sleep_for(std::chrono::milliseconds(time));
		*this->signal = true;
	}
private:
	bool* signal;
	int time;
};

/*
 * This test is a little fragile
 */
TEST(ThreadPool, Work)
{
	thread_pool pool;
	pool.initialize(0);

	bool signal = false;

	test_work work(0, &signal);

	ASSERT_FALSE(signal);

	pool.add_work(&work);

	pool.wait();

	ASSERT_TRUE(signal);
}

TEST(ThreadPool, Wait)
{
	thread_pool pool;
	pool.initialize(0);

	bool signal = false;
	bool long_signal = false;

	test_work work(0, &signal);
	test_work long_work(100, &long_signal);

	ASSERT_FALSE(signal);

	pool.add_work(&long_work);
	pool.add_work(&long_work);
	pool.add_work(&work);
	pool.add_work(&work);
	pool.add_work(&work);
	pool.add_work(&work);

	pool.wait();

	ASSERT_TRUE(signal);
	ASSERT_TRUE(long_signal);
}
