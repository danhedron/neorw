#include <gtest/gtest.h>

#include <memory.hpp>
#include <memory/system_memory_allocator.hpp>

struct SystemMemoryAllocatorSwitch {
	bool& a;
	SystemMemoryAllocatorSwitch(bool& _a)
		: a(_a) { a = true; }
	~SystemMemoryAllocatorSwitch() {
		a = false; }
};

using namespace gk;

TEST(SystemMemoryAllocator, RawAllocation) {
	system_memory_allocator alloc;

	mem_block mem = alloc.allocate(1024);

	ASSERT_NE( nullptr, mem.ptr );

	alloc.deallocate(mem);
}

TEST(SystemMemoryAllocator, TrivialAllocation) {
	system_memory_allocator alloc;

	auto mem = construct<int>( alloc );

	ASSERT_NE( nullptr, mem );

	deconstruct(alloc, mem);
}

TEST(SystemMemoryAllocator, ConstructAllocation) {
	system_memory_allocator alloc;

	bool val = false;

	auto mem = construct<SystemMemoryAllocatorSwitch>(alloc, val);

	ASSERT_TRUE(val);

	deconstruct(alloc, mem);

	ASSERT_FALSE(val);
}
