#include <gtest/gtest.h>

#include <memory.hpp>
#include <memory/system_memory_allocator.hpp>

#include "test_main.hpp"

using namespace gk;

TEST(MemoryPool, TrivialAllocation) {
	memory_pool pool ( &global_pool(), memory_pool::unbounded );

	mem_block mem = pool.allocate(512);

	ASSERT_NE( nullptr, mem.ptr );

	pool.deallocate( mem );
}

TEST(MemoryPool, Size) {
	memory_pool pool ( &global_pool(), 512 );

	mem_block mem1 = pool.allocate(128);
	ASSERT_NE( nullptr, mem1.ptr );

	mem_block mem2 = pool.allocate(1024);
	ASSERT_EQ( nullptr, mem2.ptr );

	mem_block mem3 = pool.allocate(256);
	ASSERT_NE( nullptr, mem3.ptr );

	mem_block mem4 = pool.allocate(256);
	ASSERT_EQ( nullptr, mem4.ptr );

	pool.deallocate(mem3);

	mem_block mem5 = pool.allocate(256);
	ASSERT_NE( nullptr, mem5.ptr );

	pool.deallocate(mem1);
	pool.deallocate(mem5);
}

TEST(MemoryPool, Statistics) {
	memory_pool pool ( &global_pool(), memory_pool::unbounded );

	ASSERT_EQ( 0, pool.maximum_allocation() );
	ASSERT_EQ( 0, pool.current_allocation() );
	ASSERT_EQ( memory_pool::unbounded, pool.capacity() );

	mem_block mem = pool.allocate(512);

	ASSERT_NE( nullptr, mem.ptr );
	ASSERT_GE( pool.current_allocation(), 512 );
	ASSERT_GE( pool.maximum_allocation(), 512 );

	pool.deallocate( mem );

	ASSERT_EQ( pool.current_allocation(), 0 );
	ASSERT_GE( pool.maximum_allocation(), 512 );
}

TEST(MemoryPool, Name) {
	memory_pool pool ( &global_pool(), memory_pool::unbounded, "TEST_POOL" );

	ASSERT_STREQ( "TEST_POOL", pool.name() );
}
