#include <gtest/gtest.h>

#include <fs/file_info.hpp>

using namespace gk;

//
// These tests assume a few things that are not going to be true on windows,
// more reliable tests are needed.
//

TEST(FileInfo, Exists)
{
	{
		file_info file("/tmp");
		ASSERT_TRUE(file.exists());
	}

	{
		file_info file("/tmp-notexisting");
		ASSERT_FALSE(file.exists());
	}
}

TEST(FileInfo, Sanitise)
{
	{
		file_info file("/");
		ASSERT_STREQ("/", file.path());
	}
	{
		file_info file("/tmp/neorw/");
		ASSERT_STREQ("/tmp/neorw", file.path());
	}
	{
		file_info file("/tmp/");
		ASSERT_STREQ("/tmp", file.path());
	}
}

TEST(FileInfo, Child)
{
	{
		file_info file("/");
		ASSERT_STREQ("/", file.path());

		file_info tmp_file = file.child("tmp");
		ASSERT_STREQ("/tmp", tmp_file.path());

		file_info data_file = tmp_file.child("data");
		ASSERT_STREQ("/tmp/data", data_file.path());
	}
}
