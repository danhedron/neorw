#include "gtest/gtest.h"
#include "test_main.hpp"
#include <iostream>

#include <memory/system_memory_allocator.hpp>
using namespace gk;
system_memory_allocator test_alloc;

memory_pool &global_pool()
{
	static memory_pool pool(&test_alloc, memory_pool::unbounded, "TEST");
	return pool;
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	auto result = RUN_ALL_TESTS();
	std::cout << "Memory statistics" << std::endl;
	std::cout << "maximum_allocation = "
		<< global_pool().maximum_allocation() << " bytes" << std::endl;
	std::cout << "current_allocation = "
		<< global_pool().current_allocation() << " bytes" << std::endl;
	return result;
}
