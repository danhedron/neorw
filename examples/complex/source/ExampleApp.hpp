#ifndef EXAMPLEAPP_HPP_
#define EXAMPLEAPP_HPP_
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <job_system.hpp>
#include <memory.hpp>

class DrawStep;
class DebugDrawStep;
class ParticleManager;

class ExampleApp {
public:
	ExampleApp();
	~ExampleApp();

	int run();

	void tick();
private:
	sf::RenderWindow m_wnd;
	gk::thread_pool m_pool;

	gk::memory_pool m_poolDraw;

	gk::mem_owned<ParticleManager> m_particles;
#if NEORW_DEBUG
	gk::mem_owned<DebugDrawStep> m_debugDraw;
#endif

	// List of drawers to use (temporary location)
	std::vector<DrawStep*> m_drawSteps;
};

#endif
