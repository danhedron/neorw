#include "DebugDrawStep.hpp"
#include <core/types.hpp>
#include <core/debug.hpp>

#include <sstream>
#include <iomanip>
#include <algorithm>

static const GKChar *cDebugFontFile = "resources/Roboto-Light.ttf";
static const int cDebugSize = 14;
static const sf::Vector2f cDebugOffset(10.f, 10.f);

DebugDrawStep* DebugDrawStep::s_instance = nullptr;

DebugDrawStep::DebugDrawStep()
	: m_tickTimer{0.f}
	, m_frameTimer{0.f}
	, m_drawTimer{0.f}
	, m_particleCount{0}
{
	GK_ASSERT(s_instance == nullptr,
			  "DebugDrawInstance singleton instance is not null!");
	s_instance = this;

	// Load the font to use
	if (!m_debugFont.loadFromFile(cDebugFontFile)) {
		GK_LOG("Error loading debug font %s", cDebugFontFile);
	}
}

DebugDrawStep::~DebugDrawStep()
{

}

void DebugDrawStep::render(sf::RenderTarget &target)
{
	float avg_frameTime = std::accumulate(std::begin(m_frameTimer), std::end(m_frameTimer), 0.f) / cFrameSmoothCount;
	float avg_tickTime = std::accumulate(std::begin(m_tickTimer), std::end(m_tickTimer), 0.f) / cFrameSmoothCount;
	float avg_drawTime = std::accumulate(std::begin(m_drawTimer), std::end(m_drawTimer), 0.f) / cFrameSmoothCount;

	std::ostringstream ss;
	ss << std::fixed << std::setprecision(3);
	ss << "workers: " << m_workers << " threads\n";
	ss << "Particles: " << m_particleCount << "\n";
	if (m_pools.size() > 0) {
		ss << "Memory Pools:\n";
		for (gk::memory_pool* pool : m_pools) {
			ss << "\t"
			   << pool->name() << "\t["
			   << "cur:" << pool->current_allocation()/1000.f << "kb "
			   << "max:" << pool->maximum_allocation()/1000.f << "kb]\n";
		}
	}
	ss << "frame: " << m_frameTimer[0] << "ms (" << avg_frameTime << "ms)\n";
	ss  << "\ttick: " << m_tickTimer[0] << "ms (" << avg_tickTime << "ms)\n";
	ss  << "\tdraw: " << m_drawTimer[0] <<"ms (" << avg_drawTime << "ms)\n";

	for (int i = cFrameSmoothCount-2; i >= 0; --i) {
		m_frameTimer[i+1] = m_frameTimer[i];
		m_tickTimer[i+1] = m_tickTimer[i];
		m_drawTimer[i+1] = m_drawTimer[i];
	}

	sf::Text debugInfoText(ss.str(), m_debugFont, cDebugSize);
	debugInfoText.setStyle(sf::Text::Bold);

	debugInfoText.setColor(sf::Color::Black);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f(-1.f,-1.f));
	target.draw(debugInfoText);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f(-1.f, 1.f));
	target.draw(debugInfoText);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f( 1.f,-1.f));
	target.draw(debugInfoText);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f( 1.f, 1.f));
	target.draw(debugInfoText);

	debugInfoText.setPosition(cDebugOffset + sf::Vector2f( 0.f,-1.f));
	target.draw(debugInfoText);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f( 0.f, 1.f));
	target.draw(debugInfoText);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f(-1.f, 0.f));
	target.draw(debugInfoText);
	debugInfoText.setPosition(cDebugOffset + sf::Vector2f( 1.f, 0.f));
	target.draw(debugInfoText);

	debugInfoText.setPosition(cDebugOffset);
	debugInfoText.setColor(sf::Color::White);
	target.draw(debugInfoText);
}
