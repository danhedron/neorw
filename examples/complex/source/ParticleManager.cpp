#include "ParticleManager.hpp"
#include "DebugDrawStep.hpp"

#include <core/debug.hpp>

static const GKChar*	cCloudImage		= "resources/cloud.png";
static const GKChar*	cRainImage		= "resources/raindrop.png";
static const GKSize_t	cInitParticles	= 50;
static const float		cFreeDeadThreshold = 0.25f;

/**
 * Returns a random float
 */
float randf(GKReal mid, GKReal spread) {
	return (rand()/(RAND_MAX*1.f) - 0.5f) * spread + mid;
}

/**
 * Returns a 2D vector with random X and Y values
 */
sf::Vector2f rand2D(GKReal mid, GKReal spread) {
	return {
		randf(mid, spread),
		randf(mid, spread)
	};
}

/**
 * Returns a 2D vector with the same random value for X and Y
 */
sf::Vector2f rand1D(GKReal mid, GKReal spread) {
	GKReal val = randf(mid, spread);
	return { val, val };
}

ParticleUpdateTask::ParticleUpdateTask(GKReal step,
									   ParticleData *data,
									   GKSize_t count)
	: job_task("ParticleUpdate")
	, m_step(step)
	, m_data(data)
	, m_particles(count)
	, m_deadCount(0)
{

}

void ParticleUpdateTask::work()
{
	ParticleData *pParticle = m_data;
	for (GKSize_t p = 0; p < m_particles; ++p, pParticle++) {
		pParticle->position += pParticle->velocity * m_step;
		pParticle->age += m_step;
		if(pParticle->age >= pParticle->lifetime) {
			m_deadCount ++;
		}
	}
}

ParticleManager::ParticleManager(gk::memory_allocator* alloc,
								 gk::thread_pool &pool)
	: m_allocator(alloc, gk::memory_pool::unbounded, "Particles")
	, m_pool(pool)
	, m_particles(&m_allocator)
{
	DebugDrawStep::getInstance()->m_pools.push_back(&m_allocator);
	m_cloudImage.loadFromFile(cCloudImage);
	m_rainImage.loadFromFile(cRainImage);

	m_particles.reserve(cInitParticles);
	for (int i = 0; i < cInitParticles; ++i) {
		m_particles.push_back({
								  (rand() % 2),
								  rand2D(250.f, 500.f),
								  rand2D(0.f, 10.f),
								  rand1D(1.0f, 0.1f),
								  0.f,
								  randf(5.f, 2.f)
							  });
	}
}

void ParticleManager::update(GKReal step) {
#if NEORW_DEBUG
	DebugDrawStep::getInstance()->m_particleCount = m_particles.size();
#endif

	// Check we have any work to do:-
	if (m_particles.size() == 0) {
		return;
	}

	// Split work for each hw thread & create tasks

	GKSize_t numWorkers = m_pool.worker_count();
	std::vector<ParticleUpdateTask> tasks;

	// There's no sense in creating a taks for every particle
	// Submit all as one job.
	if (numWorkers >= m_particles.size()) {
		tasks.push_back(ParticleUpdateTask(
							step,
							m_particles.data(),
							m_particles.size()));
	}
	else {
		GKSize_t batchSize = (m_particles.size() / numWorkers) + 1;
		for (GKSize_t batch = 0;
			 batch < m_particles.size();
			 batch += batchSize) {
			GKSize_t remaining = (m_particles.size() - batch);
			tasks.push_back(ParticleUpdateTask(
								step,
								m_particles.data() + batch,
								std::min(batchSize, remaining)));
		}
	}

	// Push all work
	for (ParticleUpdateTask& task : tasks) {
		m_pool.add_work(&task);
	}

	// Wait for tasks to complete before continuing
	m_pool.wait();

	// Do some post-update book-keeping
	GKSize_t deadCount = 0;
	for (ParticleUpdateTask& task : tasks) {
		deadCount += task.getDead();
	}

	if (deadCount > m_particles.size() * cFreeDeadThreshold) {
		// A significant number of particles in our care have died.
		// To avoid an inquiry, clean them up and downsize.
		std::sort(m_particles.begin(), m_particles.end(),
				  [](const ParticleData& a, const ParticleData& b) {
					return (a.age >= a.lifetime) < (b.age >= b.lifetime);
		});
		m_particles.resize(m_particles.size() - deadCount);
	}

	// Spawn some particles to make up for the the fact that there's nothing
	// interesting happening
	if (m_particles.size()-deadCount < cInitParticles) {
		spawnRandomParticle();
		spawnRandomParticle();
		spawnRandomParticle();
	}
}

void ParticleManager::spawnRandomParticle() {
	// Find the first free slot
	GKReal reuseIdx = m_particles.size();
	for (GKSize_t p = 0; p < m_particles.size(); ++p) {
		if(m_particles[p].age >= m_particles[p].lifetime) {
			reuseIdx = p;
			break;
		}
	}
	if (reuseIdx == m_particles.size()) {
		m_particles.resize(reuseIdx+1);
	}
	m_particles[reuseIdx] = {
		(rand() % 2),
		rand2D(250.f, 500.f),
		rand2D(0.f, 10.f),
		rand1D(1.0f, 0.1f),
		0.f,
		randf(5.f, 2.f)
	};
}

void ParticleManager::render(sf::RenderTarget &target) {
	for (GKSize_t p = 0; p < m_particles.size(); ++p) {

		if (m_particles[p].age >= m_particles[p].lifetime) {
			// Dead Particle
			continue;
		}

		switch (m_particles[p].material) {
		case 0:
			m_sprite.setTexture(m_cloudImage);
			m_sprite.setTextureRect(
						sf::IntRect(sf::Vector2i(0, 0),
									sf::Vector2i(m_cloudImage.getSize())));
			break;
		case 1:
			m_sprite.setTexture(m_rainImage);
			m_sprite.setTextureRect(
						sf::IntRect(sf::Vector2i(0, 0),
									sf::Vector2i(m_rainImage.getSize())));
		default:
			break;
		}
		m_sprite.setPosition(m_particles[p].position);
		m_sprite.setScale(m_particles[p].scale);
		target.draw(m_sprite);
	}
}
