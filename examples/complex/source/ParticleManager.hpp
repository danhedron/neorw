#ifndef PARTICLEMANAGER_HPP
#define PARTICLEMANAGER_HPP
#include <job_system.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include "DrawStep.hpp"
#include <memory.hpp>
#include <memory/stl_allocator.hpp>

namespace gk {
	template <class T>
	using vector = std::vector<T, stl_allocator<T>>;
}

struct ParticleData {
	std::uint32_t material;
	sf::Vector2f position;
	sf::Vector2f velocity;
	sf::Vector2f scale;
	float age;
	float lifetime;
};

class ParticleUpdateTask : public gk::job_task {
public:
	ParticleUpdateTask(GKReal step, ParticleData* data, GKSize_t count);

	virtual void work() override;

	GKSize_t getDead() const { return m_deadCount; }
private:
	GKReal m_step;
	ParticleData *m_data;
	GKSize_t m_particles;
	GKSize_t m_deadCount;
};

class ParticleManager : public DrawStep
{
public:
	ParticleManager(gk::memory_allocator* alloc, gk::thread_pool& pool);

	void update(GKReal step);

	void spawnRandomParticle();

	virtual void render(sf::RenderTarget& target);

private:
	gk::memory_pool m_allocator;
	gk::thread_pool& m_pool;
	gk::vector<ParticleData> m_particles;
	sf::Sprite m_sprite;
	sf::Texture m_cloudImage;
	sf::Texture m_rainImage;
};

#endif
