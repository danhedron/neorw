#ifndef DEBUGDRAWSTEP_HPP
#define DEBUGDRAWSTEP_HPP
#include "DrawStep.hpp"
#include <core.hpp>

/**
 * @brief Draws debug information to the screen
 *
 * This is a singleton as it's accessed from various places to
 * set metrics about what's going on.
 */
class DebugDrawStep : public DrawStep
{
public:
	DebugDrawStep();
	~DebugDrawStep();

	static constexpr GKSize_t cFrameSmoothCount = 30;

	virtual void render(sf::RenderTarget& target) override;

	// Displayed metrics
	GKReal m_tickTimer[cFrameSmoothCount];
	GKReal m_frameTimer[cFrameSmoothCount];
	GKReal m_drawTimer[cFrameSmoothCount];
	GKSize_t m_workers;
	GKSize_t m_particleCount;
	std::vector<gk::memory_pool*> m_pools;

	static DebugDrawStep* getInstance() {
		return s_instance;
	}

private:
	sf::Font m_debugFont;
	static DebugDrawStep* s_instance;
};

#endif
