#ifndef DRAWSTEP_HPP
#define DRAWSTEP_HPP
#include <SFML/Graphics.hpp>
#include <memory.hpp>

/**
 * @brief Interface for drawing thingss
 */
class DrawStep
{
public:
	virtual ~DrawStep() = default;

	virtual void render(sf::RenderTarget& target) = 0;
};

#endif
