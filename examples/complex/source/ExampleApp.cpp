#include "ExampleApp.hpp"
#include <core.hpp>

#include <memory/system_memory_allocator.hpp>

#include "DebugDrawStep.hpp"
#include "ParticleManager.hpp"

static const GKReal cFixedStep = (1.f/60.f);

// Global allocator
gk::system_memory_allocator g_memoryAllocator;

ExampleApp::ExampleApp()
	: m_poolDraw(&g_memoryAllocator, gk::memory_pool::unbounded, "DrawSteps")
{
	// Initialise worker thread with auto thread count
	m_pool.initialize(0);

#if NEORW_DEBUG
	m_debugDraw = gk::construct<DebugDrawStep>(m_poolDraw);
	m_debugDraw->m_workers = m_pool.worker_count();
	m_debugDraw->m_pools.push_back(&m_poolDraw);
#endif

	// Drawing should be split from particle manager to
	// particle drawstep so we can isolate their allocations
	m_particles = gk::construct<ParticleManager>(m_poolDraw, &m_poolDraw, m_pool);
}

ExampleApp::~ExampleApp() {
	gk::deconstruct<ParticleManager>(m_poolDraw, m_particles);
	gk::deconstruct<DebugDrawStep>(m_poolDraw, m_debugDraw);
}

int ExampleApp::run() {
	// Window creation could be handled elsewhere..
	sf::VideoMode mode(800 ,600);
	m_wnd.create(mode, "neorw Demo");
	m_wnd.setVerticalSyncEnabled(false);

	// Install the draw steps for this application
	m_drawSteps.push_back(m_particles);
#if NEORW_DEBUG
	m_drawSteps.push_back(m_debugDraw);
#endif

	sf::Clock timer;
#if NEORW_DEBUG
	sf::Clock tickTimer;
	sf::Clock frameTimer;
	sf::Clock drawTimer;
#endif
	GKReal accumulator = 0.f;

	while (m_wnd.isOpen()) {
		sf::Event ev;
		while (m_wnd.pollEvent(ev)) {
			if (ev.type == sf::Event::Closed)
				m_wnd.close();
		}

		GKReal seconds = timer.restart().asSeconds();
		accumulator += seconds;

		if (accumulator > cFixedStep) {
#if NEORW_DEBUG
			tickTimer.restart();
#endif
			tick();
			accumulator -= cFixedStep;
#if NEORW_DEBUG
			m_debugDraw->m_tickTimer[0] =
					tickTimer.restart().asMicroseconds()/1000.f;
#endif
		}

#if NEORW_DEBUG
		drawTimer.restart();
#endif
		m_wnd.clear(sf::Color(50, 50, 50, 255));
		for(DrawStep* step : m_drawSteps) {
			step->render(m_wnd);
		}

		m_wnd.display();
#if NEORW_DEBUG
		m_debugDraw->m_drawTimer[0]
				= drawTimer.restart().asMicroseconds()/1000.f;
		m_debugDraw->m_frameTimer[0]
				= frameTimer.restart().asMicroseconds()/1000.f;
#endif
	}

	return 0;
}

void ExampleApp::tick()
{
	m_particles->update(1.f/60.f);
}
