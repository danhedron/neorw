#include "renderer/opengl/openglrenderer.hpp"
#include <memory/system_memory_allocator.hpp>
#include <memory/memory_pool.hpp>
#include <SDL2/SDL.h>

extern const GKChar* g_vertexShader;
extern const GKChar* g_fragmentShader;

static gk::system_memory_allocator g_alloc;
static gk::memory_pool g_pool(&g_alloc, gk::memory_pool::unbounded, "CommandQueue");

static float g_vert_data[] = {
	0.5f, 0.5f, 0.0f,
	0.5f,-0.5f, 0.0f,
   -0.5f,-0.5f, 0.0f,
};

int main() {
	SDL_Window *wnd;
	SDL_GLContext context;

	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	wnd = SDL_CreateWindow("Render Example", 0, 0, 800, 600,
						   SDL_WINDOW_SHOWN |
						   SDL_WINDOW_OPENGL);
	context = SDL_GL_CreateContext(wnd);

	// This assumes the opengl context that is currently current.
	rr::gl::OpenGLRenderer renderer;

	// Create rendering resources

	// Create the pipeline used for rendering the triangle
	rr::PipelineDescription pipelineDesc;
	pipelineDesc.sourceType = rr::PipelineDescription::ePipelineSource_GLSL;
	pipelineDesc.stageSources[rr::PipelineDescription::eShaderStage_Vertex]
			= g_vertexShader;
	pipelineDesc.stageSources[rr::PipelineDescription::eShaderStage_Fragment]
			= g_fragmentShader;
	rr::PipelineHandle pipeline = renderer.createPipeline(pipelineDesc);

	// Query the pipeline for where to bind the texture
	rr::BindPoint pipelineTextureBindPoint = renderer.queryPipelineTextureBindPoint(pipeline, "u_tex");

	// Create a texture to overlay on the triangle
	GKuint32 data[] = {
		0xFF777777, 0xFFFFFFFF,
		0xFFFFFFFF, 0xFF777777
	};

	rr::TextureDescription desc;
	desc.width = 2;
	desc.height = 2;
	desc.textureFormat = rr::TextureDescription::eTextureFormat_RGBA8;
	desc.dataType = rr::TextureDescription::eDataType_UBYTE;
	desc.dataFormat = rr::TextureDescription::eDataFormat_RGBA;
	desc.magFilter = rr::TextureDescription::eFilter_Nearest;
	desc.minFilter = rr::TextureDescription::eFilter_Linear;
	desc.data = (GKChar*)data;

	rr::TextureHandle textureHandle = renderer.createTextureResource(desc);

	// Create a buffer to store the vertex position data
	rr::BufferDescription bufferDesc;
	bufferDesc.size = sizeof(g_vert_data);
	bufferDesc.usage = rr::BufferDescription::eBufferUsage_Vertex_Arrays;
	bufferDesc.data = g_vert_data;
	rr::BufferHandle vertBuffer = renderer.createBufferResource(bufferDesc);

	// Create a 2nd buffer to store the vertex colour data, it could be stored in
	// the 1st buffer, but this is an example so let's go crazy
	rr::BufferDescription colourDesc;
	colourDesc.size = sizeof(g_vert_data);
	colourDesc.usage = rr::BufferDescription::eBufferUsage_Vertex_Arrays;
	colourDesc.data = g_vert_data;
	rr::BufferHandle colourBuffer  = renderer.createBufferResource(colourDesc);

	// Initial colour data
	float colours[] = {
		1.f, 0.f, 0.f,
		0.f, 1.f, 0.f,
		0.f, 0.f, 1.f
	};

	// Re-use this command queue for each frame
	rr::CommandQueue drawing (&g_pool);
	// This is magic - colours will be updated during each draw
	drawing.push_back(rr::Command::createUploadBufferData(colourBuffer,
														  sizeof(colours),
														  0,
														  colours));
	drawing.push_back(rr::Command::createChangePipeline(pipeline));
	drawing.push_back(rr::Command::createUseTexture(pipelineTextureBindPoint,
													textureHandle));
	drawing.push_back(rr::Command::createChangeVertexBuffer(vertBuffer,
															0, sizeof(float)*3, 0,
															3, rr::eArrayType_Float32));
	drawing.push_back(rr::Command::createChangeVertexBuffer(colourBuffer,
															1, sizeof(float)*3,
															0,
															3, rr::eArrayType_Float32));
	drawing.push_back(rr::Command::createDrawArrays(rr::eGeometryMode_Triangles, 0, 3));

	float f = 0.f;
	bool running = true;
	while (running) {
		SDL_Event ev;
		if (SDL_PollEvent(&ev)) {
			if (ev.type == SDL_QUIT) {
				running = false;
			}
		}

		f += 0.01f;
		for (size_t i = 0; i < sizeof(colours)/sizeof(colours[0]); i+=3)
		{
			colours[i + 0] = 0.5f + 0.5f * cos(f + (3.1415f*0.5f)*(i+0));
			colours[i + 1] = 0.5f + 0.5f * cos(f + (3.1415f*0.5f)*(i+1));
			colours[i + 2] = 0.5f + 0.5f * cos(f + (3.1415f*0.5f)*(i+2));
		}

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		renderer.executeCommandQueue(drawing);

		SDL_GL_SwapWindow(wnd);
	}

	printf("Memory Stats\n");
	printf("%s: %lu bytes [max %lu bytes]\n", g_pool.name(), g_pool.current_allocation(), g_pool.maximum_allocation());

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(wnd);

	return 0;
}
