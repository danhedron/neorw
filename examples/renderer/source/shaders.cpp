#include <core/types.hpp>

const GKChar* g_vertexShader
= R"(
#version 330
layout(location = 0) in vec2 v_position;
layout(location = 1) in vec3 v_colour;

out vec3 colour;

void main() {
	gl_Position = vec4(v_position, 0.0, 1.0);
	colour = v_colour;
}
)";

const GKChar* g_fragmentShader
= R"(
#version 330
uniform sampler2D u_tex;
in vec3 colour;
out vec4 f_out;
void main() {
	f_out = vec4(texture(u_tex, gl_FragCoord.xy/100.0).rgb * colour, 1.0);
}

)";
