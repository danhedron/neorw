/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef RESOURCES_HPP
#define RESOURCES_HPP
#include <core/types.hpp>

namespace rr {

typedef GKSize_t TextureHandle;
typedef GKSize_t BufferHandle;
typedef GKSize_t PipelineHandle;

/**
 * Describes the contents of a pipeline for pipeline creation
 */
struct PipelineDescription {
	enum PipelineSource {
		ePipelineSource_Unknown,

		/**
		  * The pipeline is provided as GLSL source
		  */
		ePipelineSource_GLSL,

		/**
		  * (not yet used) the pipeline is provided as compiled SPIR-V bytecode
		  */
		ePipelineSource_SPIRV,
	};
	enum ShaderStage {
		eShaderStage_Vertex = 0,
		eShaderStage_Fragment = 1,
		eShaderStage_Geometry = 2,
		_eShaderStage_Count
	};

	/**
	 * @brief sourceType The shader representation stored in stageSources
	 */
	PipelineSource sourceType;
	/**
	 * @brief stageSources contain the representation of each shader stage.
	 * The format of the representation is determined by sourceType
	 */
	const GKChar* stageSources[_eShaderStage_Count];

	PipelineDescription()
		: sourceType { ePipelineSource_Unknown }
		, stageSources { }
	{ }
};

/**
 * @brief The TextureDescription struct
 */
struct TextureDescription {
	enum TextureSource {
		/**
		  * Texture is backed by texture data
		  */
		eTextureSource_Data,
		/**
		  * Texture is backed by a buffer
		  * (not yet supported!)
		  */
		eTextureSource_Buffer,
	};
	enum TextureFormat {
		eTextureFormat_R8 = 0,
		eTextureFormat_RG8,
		eTextureFormat_RGBA8,

		eTextureFormat_R32F,
		eTextureFormat_RG32F,
		eTextureFormat_RGBA32F,

		_eTextureFormat_Count
	};
	enum DataFormat {
		eDataFormat_R = 0,
		eDataFormat_RG,
		eDataFormat_RGB,
		eDataFormat_BGR,
		eDataFormat_RGBA,
		eDataFormat_BGRA,
		eDataFormat_DEPTH,
		eDataFormat_DEPTH_STENCIL,

		_eDataFormat_Count
	};
	enum DataType {
		eDataType_UBYTE,
		eDataType_USHORT,
		eDataType_UINT,

		eDataType_FLOAT,

		_eDataType_Count
	};
	enum Filter {
		eFilter_Linear,
		eFilter_Nearest,

		_eFilter_Count
	};

	TextureFormat textureFormat;
	Filter magFilter;
	Filter minFilter;

	GKSize_t width;
	GKSize_t height;

	DataType dataType;
	DataFormat dataFormat;
	void* data;
};

/**
 * @brief The BufferDescription struct
 */
struct BufferDescription {
	enum BufferUsage {
		/** Storage for Vertex data */
		eBufferUsage_Vertex_Arrays,
		/** Storage for Index data */
		eBufferUsage_Elements,

		_eBufferUsage_Count
	};

	GKSize_t size;
	BufferUsage usage;
	void* data;
};

typedef GKint32 BindPoint;
}

#endif
