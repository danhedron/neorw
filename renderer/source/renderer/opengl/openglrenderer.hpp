/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef OPENGLRENDERER_HPP
#define OPENGLRENDERER_HPP
#include <renderer/rendercontext.hpp>
#include <renderer/commandqueue.hpp>
#include <renderer/opengl/gl_core_3_3.h>

namespace rr {
namespace gl {

class OpenGLRenderer : public RenderContext
{
public:
	/**
	 * Create an OpenGL Render Context from the GL context that is
	 * current on the thread.
	 */
	OpenGLRenderer();

	PipelineHandle createPipeline(const PipelineDescription &);
	BindPoint queryPipelineTextureBindPoint(const PipelineHandle, const GKChar *name);

	TextureHandle createTextureResource(const TextureDescription &);

	BufferHandle createBufferResource(const BufferDescription &);

	bool executeCommandQueue(CommandQueue &queue);
private:
	bool executeCommandDraw(const Command::Draw&);
	bool executeCommandDrawArrays(const Command::DrawArrays&);
	bool executeCommandChangePipeline(const Command::ChangePipeline&);
	bool executeCommandUseTexture(const Command::UseTexture&);
	bool executeCommandChangeVertexBuffer(const Command::ChangeVertexBuffer&);
	bool executeCommandUploadBufferData(const Command::UploadBufferData&);
	bool executeCommandUploadTextureData(const Command::UploadTextureData&);

private:
	GLuint m_defaultVAO;

	// Cached States
	PipelineHandle m_currentProgram;
};

}
}

#endif
