#include "openglrenderer.hpp"
#include "gl_core_3_3.h"
#include <core/debug.hpp>

using namespace rr;
using namespace rr::gl;

// Utility methods to map Renderer enums to GL enums

GLenum ShaderStageToEnum(PipelineDescription::ShaderStage stage)
{
	static const GLenum stages[] = {
		GL_VERTEX_SHADER,
		GL_FRAGMENT_SHADER,
		GL_GEOMETRY_SHADER
	};
	static_assert((sizeof(stages)/sizeof(stages[0])) == PipelineDescription::_eShaderStage_Count,
				  "stages has wrong size. update to match PipelineDescription::ShaderStage");

	if (stage < 0 || stage >= PipelineDescription::_eShaderStage_Count) return 0;
	return stages[stage];
}

GLenum TextureFormatToInternalFormat(TextureDescription::TextureFormat format)
{
	static const GLenum formats[] = {
		GL_R8,
		GL_RG8,
		GL_RGBA8,

		GL_R32F,
		GL_RG32F,
		GL_RGBA32F,
	};
	static_assert((sizeof(formats)/sizeof(formats[0])) == TextureDescription::_eTextureFormat_Count,
				  "formats has wrong size. Update to match TextureDescription::TextureFormat");

	if (format < 0 || format >= TextureDescription::_eTextureFormat_Count) return 0;
	return formats[format];
}

GLenum DataTypeToType(TextureDescription::DataType type)
{
	static const GLenum types[] = {
		GL_UNSIGNED_BYTE,
		GL_UNSIGNED_SHORT,
		GL_UNSIGNED_INT,
		GL_FLOAT
	};
	static_assert((sizeof(types)/sizeof(types[0])) == TextureDescription::_eDataType_Count,
				  "types has wrong size. Update to match TextureDescription::DataType");

	if (type < 0 || type >= TextureDescription::_eDataType_Count) return 0;
	return types[type];
}

GLenum DataFormatToFormat(TextureDescription::DataFormat fmt)
{
	static const GLenum formats[] = {
		GL_RED,
		GL_RG,
		GL_RGB,
		GL_BGR,
		GL_RGBA,
		GL_BGRA,
		GL_DEPTH_COMPONENT,
		GL_DEPTH_STENCIL
	};

	static_assert((sizeof(formats)/sizeof(formats[0])) == TextureDescription::_eDataFormat_Count,
				  "types has wrong size. Update to match TextureDescription::DataFormat");
	if (fmt < 0 || fmt >= TextureDescription::_eDataFormat_Count) return 0;
	return formats[fmt];
}

GLenum FilterToFilter(TextureDescription::Filter filter)
{
	static const GLenum filters[] = {
		GL_LINEAR,
		GL_NEAREST
	};

	static_assert((sizeof(filters)/sizeof(filters[0])) == TextureDescription::_eFilter_Count,
				  "filters has wrong size. Update to match TextureDescription::Filters");
	GK_ASSERT(filter >= 0 && filter < TextureDescription::_eFilter_Count,
			  "Invalid Filter, out of range");
	return filters[filter];
}

GLenum BufferUsageToTarget(BufferDescription::BufferUsage usage)
{
	static const GLenum targets[] = {
		GL_ARRAY_BUFFER,
		GL_ELEMENT_ARRAY_BUFFER
	};

	static_assert((sizeof(targets)/sizeof(targets[0])) == BufferDescription::_eBufferUsage_Count,
				  "types has wrong size. Update to match BufferDescription::BufferUsage");
	if (usage < 0 || usage >= BufferDescription::_eBufferUsage_Count) return 0;
	return targets[usage];
}

GLenum GeometryModeToMode(GeometryMode geometrymode)
{
	static const GLenum modes[] = {
		GL_POINTS,
		GL_LINE_STRIP,
		GL_LINE_LOOP,
		GL_LINES,
		GL_TRIANGLE_STRIP,
		GL_TRIANGLE_FAN,
		GL_TRIANGLES
	};

	static_assert((sizeof(modes)/sizeof(modes[0])) == _eGeometryMode_Count,
			"modes has wrong size, Update to match GeometryMode");
	GK_ASSERT(geometrymode >= 0 && geometrymode < _eGeometryMode_Count,
			  "Invalid GeometryMode, out of range");
	return modes[geometrymode];
}



OpenGLRenderer::OpenGLRenderer()
	: m_defaultVAO(0)
	, m_currentProgram(0)
{
	// Create the default VAO that is used
	glGenVertexArrays(1, &m_defaultVAO);
}




PipelineHandle OpenGLRenderer::createPipeline(const PipelineDescription &desc)
{
	// Can't create a pipeline without at least these, so far
	if (desc.stageSources[PipelineDescription::eShaderStage_Fragment] == nullptr ||
		desc.stageSources[PipelineDescription::eShaderStage_Vertex] == nullptr)
	{
		return 0;
	}

	GLuint program = glCreateProgram();
	bool failure = false;

	for (GKSize_t s = 0; s < PipelineDescription::_eShaderStage_Count; ++s)
	{
		if (desc.stageSources[s] == nullptr) continue;
		auto stage = static_cast<PipelineDescription::ShaderStage>(s);

		GLenum stageGl = ShaderStageToEnum(stage);
		GLuint shader = glCreateShader(stageGl);
		glShaderSource(shader, 1, &desc.stageSources[s], nullptr);
		glCompileShader(shader);
		GLint status;
		GLint logLength = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0) {
			static const GKSize_t cMaxLog = 1024;
			GLchar log[cMaxLog];
			glGetShaderInfoLog(shader, cMaxLog-1, NULL, log);
			// Should write this somewhere
			GK_LOG("Shader Infolog: %s", log);
		}
		if (status != GL_TRUE) {
			failure = true;
		}
		glAttachShader(program, shader);
		// Let GL delete this shader once program is destroyed
		glDeleteShader(shader);
	}

	if (failure)
	{
		// Don't need to clean up the shaders, they're already marked.
		glDeleteProgram(program);
		return 0;
	}

	glLinkProgram(program);
	GLint status;
	GLint logLength = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
	if (logLength > 0) {
		static const GKSize_t cMaxLog = 1024;
		GLchar log[cMaxLog];
		glGetProgramInfoLog(program, cMaxLog-1, NULL, log);
		// Should write this somewhere
		GK_LOG("Program Infolog: %s", log);
	}

	if (status != GL_TRUE) {
		glDeleteProgram(program);
		return 0;
	}

	return program;
}

BindPoint OpenGLRenderer::queryPipelineTextureBindPoint(const PipelineHandle pipeline, const GKChar *name)
{
	return glGetUniformLocation(pipeline, name);
}


TextureHandle OpenGLRenderer::createTextureResource(const TextureDescription &desc)
{
	if (desc.width == 0 || desc.height == 0)
	{
		// That ain't gonna work
		return 0;
	}

	GLuint name;
	glGenTextures(1, &name);

	// This will only support 2D textures for the time being, need to enumerate the
	// other possibilities within TextureDescription

	glBindTexture(GL_TEXTURE_2D, name);

	// Mirroring forced to repeat for now.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Set the min & mag filters to linear -- need to support mips.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterToFilter(desc.minFilter));
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterToFilter(desc.magFilter));

	// Load first level
	GLenum internalFormat = TextureFormatToInternalFormat(desc.textureFormat);
	GLenum dataFormat = DataFormatToFormat(desc.dataFormat);
	GLenum dataType = DataTypeToType(desc.dataType);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, desc.width, desc.height,
				 0, dataFormat, dataType, desc.data);

	glGenerateMipmap(GL_TEXTURE_2D);

	return name;
}

BufferHandle OpenGLRenderer::createBufferResource(const BufferDescription &desc)
{
	GK_ASSERT(desc.size > 0, "Zero-sized buffers are not permitted");
	if (desc.size == 0)
	{
		return 0;
	}
	GLuint name;

	glGenBuffers(1, &name);
	GLenum target = BufferUsageToTarget(desc.usage);
	// BufferDescription needs "static", "dynamic", "stream"
	GLenum usageHint = GL_STATIC_DRAW;

	if (desc.size > 0)
	{
		glBindBuffer(target, name);
		glBufferData(target, desc.size, desc.data, usageHint);
	}

	return name;
}

bool OpenGLRenderer::executeCommandQueue(CommandQueue &queue)
{
	// so far there's no mechanism for exposing VAOs so we have a default one
	glBindVertexArray(m_defaultVAO);

	bool result = true;
	for (auto it = queue.begin(); it != queue.end(); ++it)
	{
		const auto& cmd = *it;
		switch(cmd.command) {
		case Command::eCommand_Draw:
			result &= executeCommandDraw(cmd.draw);
			break;
		case Command::eCommand_DrawArrays:
			result &= executeCommandDrawArrays(cmd.drawArrays);
			break;
		case Command::eCommand_ChangePipeline:
			result &= executeCommandChangePipeline(cmd.changePipeline);
			break;
		case Command::eCommand_UseTexture:
			result &= executeCommandUseTexture(cmd.useTexture);
			break;
		case Command::eCommand_ChangeVertexBuffer:
			result &= executeCommandChangeVertexBuffer(cmd.changeVertexBuffer);
			break;
		case Command::eCommand_UploadBufferData:
			result &= executeCommandUploadBufferData(cmd.uploadBufferData);
			break;
		case Command::eCommand_UploadTextureData:
			result &= executeCommandUploadTextureData(cmd.uploadTextureData);
			break;
		default:
			GK_LOG("Unhandled Command type!");
			break;
		}
	}
	return result;
}

bool OpenGLRenderer::executeCommandDraw(const Command::Draw &)
{
	return true;
}

bool OpenGLRenderer::executeCommandDrawArrays(const Command::DrawArrays &cmd)
{
	glDrawArrays(GeometryModeToMode(cmd.mode), cmd.first, cmd.elements);
	return true;
}

bool OpenGLRenderer::executeCommandChangePipeline(const Command::ChangePipeline &cp)
{
	// This should be shadowed so we don't change pipelines if we can help it
	if (m_currentProgram != cp.pipelineHandle)
	{
		glUseProgram(cp.pipelineHandle);
		m_currentProgram = cp.pipelineHandle;
	}
	return true;
}

bool OpenGLRenderer::executeCommandUseTexture(const Command::UseTexture &ut)
{
	// Texture state needs to be handled better!
	glActiveTexture(GL_TEXTURE0 + ut.bindPoint);
	glBindTexture(GL_TEXTURE_2D, ut.textureHandle);
	glUniform1i(ut.bindPoint, ut.bindPoint);
	return true;
}

bool OpenGLRenderer::executeCommandChangeVertexBuffer(const Command::ChangeVertexBuffer &cvb)
{
	// If a null buffer handle is passed, we can at least disable the index.
	if (cvb.bufferHandle == 0) {
		glDisableVertexAttribArray(cvb.index);
	}
	else {
		// This state should be cached by the renderer so the clients don't need to worry
		// about putting optimal state into the command queue
		glBindBuffer(GL_ARRAY_BUFFER, cvb.bufferHandle);
		glEnableVertexAttribArray(cvb.index);
		switch (cvb.type) {
		case eArrayType_Float32:
			glVertexAttribPointer(cvb.index, cvb.components,
								  GL_FLOAT, GL_FALSE, cvb.stride,
								  reinterpret_cast<const GLvoid*>(cvb.offset));
			break;
		}
	}
	return true;
}

bool OpenGLRenderer::executeCommandUploadBufferData(const Command::UploadBufferData &ubd)
{
	// Buffer is sized in advance, but no saftey checks on size here :(((
	glBindBuffer(GL_ARRAY_BUFFER, ubd.buffer);
	glBufferSubData(GL_ARRAY_BUFFER, ubd.offset, ubd.size, ubd.data);
	return true;
}

bool OpenGLRenderer::executeCommandUploadTextureData(const Command::UploadTextureData &utd)
{
	glBindTexture(GL_TEXTURE_2D, utd.texture);

	GLenum internalFormat = TextureFormatToInternalFormat(utd.desc.textureFormat);
	GLenum dataFormat = DataFormatToFormat(utd.desc.dataFormat);
	GLenum dataType = DataTypeToType(utd.desc.dataType);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, utd.desc.width, utd.desc.height,
				 0, dataFormat, dataType, utd.desc.data);

	glGenerateMipmap(GL_TEXTURE_2D);

	return true;
}
