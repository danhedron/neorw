/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef COMMANDQUEUE_HPP
#define COMMANDQUEUE_HPP
#include <core/types.hpp>
#include <memory/stl_allocator.hpp>
#include <renderer/resources.hpp>

namespace rr {

/* Temporarily located here */
enum GeometryMode {
	eGeometryMode_Points,
	eGeometryMode_LineStrip,
	eGeometryMode_LineLoop,
	eGeometryMode_Lines,
	eGeometryMode_TriangleStrip,
	eGeometryMode_TriangleFan,
	eGeometryMode_Triangles,
	/*eGeometryMode_Patches*/
	_eGeometryMode_Count
};

enum ElementSize {
	eElementSize_UByte,
	eElementSize_UShort,
	eElementSize_UInt
};

enum ArrayType {
	eArrayType_Float32
};

struct Command {
	/**
	 * @brief The CommandID enum determines what data is present
	 */
	enum CommandID {
		//============================= Drawing ===============================

		/**
		  * Perform a draw call using the current state, and the specified parameters
		  */
		eCommand_Draw,
		eCommand_DrawArrays,
		/**
		  * Change the current shader/program state
		  */
		eCommand_ChangePipeline,
		/**
		  * Use the given texture in the given shader binding slot
		  */
		eCommand_UseTexture,
		/**
		  * Use the given buffer to source vertex information for the given
		  * shader vertex index
		  */
		eCommand_ChangeVertexBuffer,

		//============================= Resource Updates ======================

		/**
		  * Uploads new data into a buffer, if the buffer is mutable.
		  */
		eCommand_UploadBufferData,

		/**
		  * Uploads new data into a texture
		  */
		eCommand_UploadTextureData,
	};

	struct Draw {
		GeometryMode mode;
		GKSize_t elements;
		ElementSize elementSize;
		GKSize_t elementsOffset;
		GKSize_t instances;
		GKint32 baseVertex;
	};

	struct DrawArrays {
		GeometryMode mode;
		GKSize_t first;
		GKSize_t elements;
	};

	struct ChangePipeline {
		PipelineHandle pipelineHandle;
	};

	struct UseTexture {
		BindPoint bindPoint;
		TextureHandle textureHandle;
	};

	struct ChangeVertexBuffer {
		BufferHandle bufferHandle;
		GKuint32 index;
		GKuint32 stride;
		GKuint32 offset;
		GKuint32 components;
		ArrayType type;
	};

	struct UploadBufferData {
		BufferHandle buffer;
		GKSize_t size;
		GKSize_t offset;
		void* data;
	};

	struct UploadTextureData {
		TextureHandle texture;
		TextureDescription desc;
	};

	CommandID command;
	union {
		Draw				draw;
		DrawArrays			drawArrays;
		ChangePipeline		changePipeline;
		UseTexture			useTexture;
		ChangeVertexBuffer	changeVertexBuffer;
		UploadBufferData	uploadBufferData;
		UploadTextureData	uploadTextureData;
	};

public:
	static Command createDraw(GeometryMode mode,
							  GKSize_t elements,
							  ElementSize elementSize,
							  GKSize_t elementsOffset,
							  GKSize_t instances,
							  GKint32 baseVertex)
	{
		Command cmd;
		cmd.command = eCommand_Draw;
		cmd.draw.mode = mode;
		cmd.draw.elements = elements;
		cmd.draw.elementSize = elementSize;
		cmd.draw.elementsOffset = elementsOffset;
		cmd.draw.instances = instances;
		cmd.draw.baseVertex = baseVertex;
		return cmd;
	}

	static Command createDrawArrays(GeometryMode mode,
									GKSize_t first,
									GKSize_t elements)
	{
		Command cmd;
		cmd.command = eCommand_DrawArrays;
		cmd.drawArrays.mode = mode;
		cmd.drawArrays.first = first;
		cmd.drawArrays.elements = elements;
		return cmd;
	}

	static Command createChangePipeline(PipelineHandle pipeline)
	{
		Command cmd;
		cmd.command = eCommand_ChangePipeline;
		cmd.changePipeline.pipelineHandle = pipeline;
		return cmd;
	}

	static Command createUseTexture(BindPoint point,
									TextureHandle texture)
	{
		Command cmd;
		cmd.command = eCommand_UseTexture;
		cmd.useTexture.bindPoint = point;
		cmd.useTexture.textureHandle = texture;
		return cmd;
	}

	static Command createChangeVertexBuffer(BufferHandle buffer,
									 GKuint32 index,
									 GKuint32 stride,
									 GKuint32 offset,
									 GKuint32 components,
									 ArrayType type)
	{
		Command cmd;
		cmd.command = eCommand_ChangeVertexBuffer;
		cmd.changeVertexBuffer.bufferHandle = buffer;
		cmd.changeVertexBuffer.index = index;
		cmd.changeVertexBuffer.stride = stride;
		cmd.changeVertexBuffer.offset = offset;
		cmd.changeVertexBuffer.components = components;
		cmd.changeVertexBuffer.type = type;
		return cmd;
	}

	static Command createUploadBufferData(BufferHandle buffer,
										  GKSize_t size,
										  GKSize_t offset,
										  void* data)
	{
		Command cmd;
		cmd.command = eCommand_UploadBufferData;
		cmd.uploadBufferData.buffer = buffer;
		cmd.uploadBufferData.size = size;
		cmd.uploadBufferData.offset = offset;
		cmd.uploadBufferData.data = data;
		return cmd;
	}

	static Command createUploadTextureData(TextureHandle texture,
										   TextureDescription desc)
	{
		Command cmd;
		cmd.command = eCommand_UploadTextureData;
		cmd.uploadTextureData.texture = texture;
		cmd.uploadTextureData.desc = desc;
		return cmd;
	}

private:
	Command() { }
};

/**
 * @brief CommandQueue A submission of commands to be executed by the renderer
 *
 * With Vulkan having a lot of work being moved into command buffer construction,
 * it will be possible to use this interface to allow applications to control
 * where this work happens, but we only have a GL renderer for now, so this is
 * just a dumb list of commands that our OpenGLRenderer can use to dispatch GL
 * calls. To that end, the contents of the queue remain unchanged after a submission.
 */
typedef gk::vector<Command> CommandQueue;

}
#endif
