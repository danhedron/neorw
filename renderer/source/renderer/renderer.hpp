/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef RENDERER_HPP
#define RENDERER_HPP

/*
 * What *is* a renderer
 * namespace: rr
 *
 * Command Queues (rr::CommandQueue, rr::Command)
 *	An ordered list of commands that change the rendering state or
 *	execute a draw call/compute dispatch
 *	Examples of things that constitude Commands:
 *		Draw (instances, baseVertex, indirectBuffer)
 *		InstanceData (upload per-draw information (matrices))
 *		ConstantData (persistent buffer)
 *		ActivatePipeline (makes a pipeline set the active one)
 *		MapBuffer (buffer, offset, size, data)
 *		UseTexture(pipeline binding, texture)
 *	Commands are a mix of high & low-level in the hopes of performing optimisation
 *	within the render context of things such as bindless textures, improved buffer
 *	management and other state vector optimisations. While at the same time, avoiding
 *	implementation details about OpenGL or Vulkan leaking into the application rendering
 *	logic.
 *
 *	ResourceDescriptions
 *	Contain information required to create/change a resource
 *	Examples:
 *		PipelineDescription contains the bytes for the individual shaders in the
 *		pipeline
 *		TextureDescription contain the information required to create a texture
 *		BufferDescription contains the information ... you get the idea
 *	Resources that have some data (Textures & Buffers) support uploading
 *	of data at creation time, or by submitting an appropriate update command
 *	in a render queue.
 *
 * Render Context (rr::RenderContext)
 *	Abstract interface over some implementation of a rasterisation API, OpenGL
 *	or a software renderer, for example.
 *	Primary use is bringing to life a command queue & resource allocation.
 *	Primary API usages:
 *		executeCommandQueue(...)
 *		createPipeline()
 *	Other useful API bits:
 *		GetRendererID() -> "OpenGL"
 *		GetRendererAPIVersion() -> "OpenGL 4.5 Core"
 *
 * OpenGL Render Context (rr::gl::GLRenderContext)
 *	Creates a render context for OpenGL rendering,
 *	Evaluates driver feature set to determine available features and chooses the
 *	optimal path for each command in the command queue.
 */

#endif
