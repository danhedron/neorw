/*
	This file is part of NeoRW

	The MIT License (MIT)

	Copyright (C) 2015 NeoRW Contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef RENDERCONTEXT_HPP
#define RENDERCONTEXT_HPP
#include "commandqueue.hpp"
#include "resources.hpp"

namespace rr {

/**
 * @brief The RenderContext interface allows access to graphics APIs
 *
 * The interface exports the minimum of functionality required to dispatch
 * graphics commands such as executing graphics commands and creating graphics
 * resources.
 */
class RenderContext {
public:

	//================================= Resources

	virtual TextureHandle	createTextureResource(const TextureDescription&) = 0;

	virtual BufferHandle	createBufferResource(const BufferDescription&) = 0;

	virtual PipelineHandle	createPipeline(const PipelineDescription&) = 0;

	//================================= Pipeline Introspection

	virtual BindPoint		queryPipelineTextureBindPoint(const PipelineHandle, const GKChar* name) = 0;

	//================================= Commands

	/**
	 * @brief executeCommandQueue execute the commands from a command queue
	 * @param queue the CommandQueue to execute commands from
	 * @return True if no errors occured, false otherwise
	 */
	bool executeCommandQueue(const CommandQueue& queue);

	/**
	 * @return True if there are outstanding errors
	 */
	bool hasError() const;

	/* not done yet  - don't rely on in release */
	GKSize_t getNextError();
};

}

#endif
