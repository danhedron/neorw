#ifndef TEST_MAIN_HPP
#define TEST_MAIN_HPP

#include <SDL2/SDL.h>

#include <memory/memory_pool.hpp>

gk::memory_pool &global_pool();

SDL_Window*		test_window();
SDL_GLContext	test_context();

#define GL_TEST() \
	SDL_GL_MakeCurrent(test_window(), test_context());

#endif
