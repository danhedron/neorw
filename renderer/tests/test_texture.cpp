#include <gtest/gtest.h>
#include "test_main.hpp"

#include "renderer/rendercontext.hpp"
#include "renderer/opengl/openglrenderer.hpp"

using namespace rr;

TEST(Texture, FailCreation) {
	/* Creating a texture with a null size is not allowed */
	GL_TEST();

	gl::OpenGLRenderer renderer;

	TextureDescription desc;
	desc.width = 0;
	desc.height = 0;
	desc.textureFormat = TextureDescription::eTextureFormat_RGBA8;
	desc.data = nullptr;

	TextureHandle hnd = renderer.createTextureResource(desc);

	ASSERT_EQ(0, hnd);
}


TEST(Texture, SucceedCreation_Null) {
	/* Creating a texture with non-null size & null data is allowed */
	GL_TEST();

	gl::OpenGLRenderer renderer;

	TextureDescription desc;
	desc.width = 64;
	desc.height = 64;
	desc.data = nullptr;

	TextureHandle hnd = renderer.createTextureResource(desc);

	ASSERT_GT(hnd, 0);
}


TEST(Texture, SucceedCreation_Data) {
	/* Creating a texture with data is allowed */
	GL_TEST();

	gl::OpenGLRenderer renderer;

	GKuint32 data[] = {
		0xFF00FFFF, 0xFF00FFFF,
		0xFF00FFFF, 0xFF00FFFF
	};

	TextureDescription desc;
	desc.width = 2;
	desc.height = 2;
	desc.textureFormat = TextureDescription::eTextureFormat_RGBA8;
	desc.dataType = TextureDescription::eDataType_UBYTE;
	desc.dataFormat = TextureDescription::eDataFormat_RGBA;
	desc.data = (GKChar*)data;

	TextureHandle hnd = renderer.createTextureResource(desc);

	ASSERT_GT(hnd, 0);
}
