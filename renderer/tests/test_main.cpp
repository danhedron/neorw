#include "gtest/gtest.h"
#include "test_main.hpp"
#include <iostream>

#include <memory/system_memory_allocator.hpp>
using namespace gk;
system_memory_allocator test_alloc;
SDL_Window* wnd;
SDL_GLContext ctx;

memory_pool &global_pool()
{
	static memory_pool pool(&test_alloc, memory_pool::unbounded, "TEST");
	return pool;
}

SDL_Window* test_window()
{
	return wnd;
}

SDL_GLContext test_context()
{
	return ctx;
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);

	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	wnd = SDL_CreateWindow("Unit Tests", 0, 0, 800, 600,
						   SDL_WINDOW_SHOWN |
						   SDL_WINDOW_OPENGL );
	ctx = SDL_GL_CreateContext(wnd);

	auto result = RUN_ALL_TESTS();
	std::cout << "Memory statistics" << std::endl;
	std::cout << "maximum_allocation = "
		<< global_pool().maximum_allocation() << " bytes" << std::endl;
	std::cout << "current_allocation = "
		<< global_pool().current_allocation() << " bytes" << std::endl;
	return result;
}
