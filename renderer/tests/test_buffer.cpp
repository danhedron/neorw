#include <gtest/gtest.h>
#include "test_main.hpp"

#include "renderer/rendercontext.hpp"
#include "renderer/opengl/openglrenderer.hpp"

// this isn't part of the public API, but we gotta test something
#include "renderer/opengl/gl_core_3_3.h"

using namespace rr;

TEST(Buffer, FailCreation) {
	/* Creating a buffer with size 0 is not permitted */
	GL_TEST();

	gl::OpenGLRenderer renderer;

	BufferDescription desc;
	desc.size = 0;
	desc.usage = BufferDescription::eBufferUsage_Vertex_Arrays;
	desc.data = nullptr;

	BufferHandle hnd = renderer.createBufferResource(desc);

	ASSERT_EQ(0, hnd);
}


TEST(Buffer, SucceedCreation_Null) {
	/* Creating a buffer with a size but no data is allowed */
	GL_TEST();

	gl::OpenGLRenderer renderer;

	BufferDescription desc;
	desc.size = 128;
	desc.usage = BufferDescription::eBufferUsage_Vertex_Arrays;
	desc.data = nullptr;

	BufferHandle hnd = renderer.createBufferResource(desc);

	ASSERT_GT(hnd, 0);
	ASSERT_TRUE(glIsBuffer(hnd));
}


TEST(Buffer, SucceedCreation_Data) {
	/* Creating a buffer with data is allowed */
	GL_TEST();

	gl::OpenGLRenderer renderer;

	GKuint32 data[] = {
		0xFF00FFFF, 0xFF00FFFF,
		0xFF00FFFF, 0xFF00FFFF
	};

	BufferDescription desc;
	desc.size = sizeof(data);
	desc.data = data;
	desc.usage = BufferDescription::eBufferUsage_Vertex_Arrays;

	BufferHandle hnd = renderer.createBufferResource(desc);

	ASSERT_GT(hnd, 0);
	ASSERT_TRUE(glIsBuffer(hnd));
}
