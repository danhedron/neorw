#include <gtest/gtest.h>
#include "test_main.hpp"

#include "renderer/rendercontext.hpp"
#include "renderer/opengl/openglrenderer.hpp"

using namespace rr;

TEST(Pipeline, FailCreation) {
	/* An Invalid Pipeline Description should return a null handle */
	GL_TEST();

	// Inherit the GL from the current context
	gl::OpenGLRenderer renderer;

	PipelineDescription desc;
	desc.sourceType = PipelineDescription::ePipelineSource_GLSL;
	desc.stageSources[PipelineDescription::eShaderStage_Vertex]
		= R"(
		#version 330
		invalid _GLSL_
		)";
	desc.stageSources[PipelineDescription::eShaderStage_Fragment]
		= R"(
		#version 330
		invalid _GLSL_
		)";
	PipelineHandle pipeline = renderer.createPipeline(desc);

	ASSERT_EQ(pipeline, 0);
}

TEST(Pipeline, SucceedCreation) {
	/* A valid trivial program should return a valid handle */
	GL_TEST();

	// Inherit the GL from the current context
	gl::OpenGLRenderer renderer;

	PipelineDescription desc;
	desc.sourceType = PipelineDescription::ePipelineSource_GLSL;
	desc.stageSources[PipelineDescription::eShaderStage_Vertex]
		= R"(#version 330
		void main() { gl_Position = vec4(0); }
		)";
	desc.stageSources[PipelineDescription::eShaderStage_Fragment]
		= R"(#version 330
		out vec4 o;
		void main() { o = vec4(1); }
		)";

	PipelineHandle pipeline = renderer.createPipeline(desc);

	ASSERT_GT(pipeline, 0);
}

TEST(Pipeline, TextureBindPoint) {
	/* A Pipeline should return a valid bind point for a texture */
	GL_TEST();

	// Inherit the GL from the current context
	gl::OpenGLRenderer renderer;

	PipelineDescription desc;
	desc.sourceType = PipelineDescription::ePipelineSource_GLSL;
	desc.stageSources[PipelineDescription::eShaderStage_Vertex]
		= R"(#version 330
		void main() { gl_Position = vec4(0); }
		)";
	desc.stageSources[PipelineDescription::eShaderStage_Fragment]
		= R"(#version 330
		uniform sampler2D u_tex;
		out vec4 o;
		void main() { o = texture(u_tex, vec2(0.5, 0.5)); }
		)";

	PipelineHandle pipeline = renderer.createPipeline(desc);

	ASSERT_GT(pipeline, 0);

	BindPoint textureBindPoint = renderer.queryPipelineTextureBindPoint(pipeline, "u_tex");

	ASSERT_NE(textureBindPoint, -1);
}

