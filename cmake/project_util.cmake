#######################################
#	Declare_Library
#######################################
function(Declare_Library)
	message("Building Library ${PROJECT_NAME}")

	add_library(${PROJECT_NAME} ${PROJECT_SOURCE})

	set_target_properties(${PROJECT_NAME} PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES     "${PROJECT_INTERFACE_PATHS}"
		COMPILE_FLAGS "${PROJECT_FLAGS} ${GLOBAL_COMPILE_FLAGS}"
		LINK_FLAGS "-pthread")

	if (DEFINED PROJECT_TEST)
		message("Building ${PROJECT_NAME} tests")
		SET(PROJECT_TEST_NAME ${PROJECT_NAME}_tests)
		add_executable(${PROJECT_TEST_NAME} ${PROJECT_TEST})
		add_test(NAME ${PROJECT_TEST_NAME} COMMAND ${PROJECT_TEST_NAME})
		# Get the exported include directory for GTest
		get_target_property(GTEST_INCLUDE_DIRS GTest INTERFACE_INCLUDE_DIRECTORIES)
		set_target_properties(${PROJECT_TEST_NAME} PROPERTIES
			COMPILE_FLAGS "${PROJECT_FLAGS}"
			LINK_FLAGS "-pthread")
		include_directories(SYSTEM ${GTEST_INCLUDE_DIRS} ${PROJECT_INCLUDE_DIRS})
		target_link_libraries(${PROJECT_TEST_NAME} GTest ${PROJECT_NAME})
	endif ()

	foreach(TARGET ${PROJECT_DEPENDANCIES})
		target_link_libraries(${PROJECT_NAME} ${TARGET})

		get_target_property(TARGET_INTERFACES ${TARGET} INTERFACE_INCLUDE_DIRECTORIES)
		include_directories(SYSTEM ${TARGET_INTERFACES})
	endforeach()
endfunction()

function(Declare_Application)
	message("Building ${PROJECT_NAME}")

	add_executable(${PROJECT_NAME} ${PROJECT_SOURCE})

	set_target_properties(${PROJECT_NAME} PROPERTIES
		COMPILE_FLAGS "${PROJECT_FLAGS} ${GLOBAL_COMPILE_FLAGS}"
		LINK_FLAGS "-pthread")

	foreach(TARGET ${PROJECT_DEPENDANCIES})
		target_link_libraries(${PROJECT_NAME} ${TARGET})

		get_target_property(TARGET_INTERFACES ${TARGET} INTERFACE_INCLUDE_DIRECTORIES)
		include_directories(SYSTEM ${TARGET_INTERFACES})
	endforeach()
endfunction()
